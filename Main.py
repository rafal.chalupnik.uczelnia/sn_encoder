import gzip
import matplotlib.pyplot as plt
import numpy as np
import pickle
import Network as net


def vectorize(x, length):
    output = []
    for i in range(len(x)):
        vector = np.zeros(length)
        vector[x[i]] = 1
        output.append(vector)
    return np.asarray(output)


def sigmoid(x):
    min = np.min(x)
    x = (x - min) / (np.max(x) - min)
    return 1 / (1 + np.exp(-x))


def d_sigmoid(x):
    e_x = np.exp(-x)
    return e_x / np.power(e_x + 1, 2)


def relu(x):
    return np.where(x > 0, x, 0)
    #return np.asarray([xi * (xi > 0) for xi in x])


def d_relu(x):
    return np.where(x <= 0, 0, 1)


def softplus(x):
    max = np.where(x > 0, x, 0)
    return np.log(1 + np.exp(-abs(x))) + max


def d_softplus(x):
    return sigmoid(x)


def mst(y, predicted):
    if y.shape != predicted.shape:
        return np.sum(np.power(predicted - vectorize(y, 10), 2)) / len(y) / predicted.shape[1]
    else:
        return np.sum(np.power(predicted - y, 2)) / len(y) / predicted.shape[1]


def log_likelihood(y, predicted):
    l = -np.log(predicted[range(len(y)), y])
    return np.sum(l / len(y))


def mst_sigmoid_gradient(z, _, y):
    if y.shape != z.shape:
        return (vectorize(y, 10) - sigmoid(z)) * d_sigmoid(z) / z.shape[1]
    else:
        return (y - sigmoid(z)) * d_sigmoid(z) / z.shape[1]


def softmax_log_gradient(_, a, y):
    zeros = np.zeros(a.shape)
    for i in range(a.shape[0]):
        for j in range(a.shape[1]):
            if j == y[i]:
                zeros[i][j] = 1 - a[i][j]
            else:
                zeros[i][j] = 0 - a[i][j]
        # zeros[i][y[i]] = a[i][y[i]]
    return zeros


def regularization_l0_cost(weights, regularization_factor, y):
    return 0


def regularization_l1_cost(weights, regularization_factor, y):
    return np.sum([np.sum(np.abs(w)) for w in weights]) * regularization_factor / len(y)


def regularization_l2_cost(weights, regularization_factor, y):
    return np.sum([np.sum(np.square(w)) for w in weights]) * regularization_factor / len(y)


def regularization_l0_gradient(weights, regularization_factor, y):
    return 0


def regularization_l1_gradient(weights, regularization_factor, y):
    return regularization_factor / len(y) * (weights / np.abs(weights))


def regularization_l2_gradient(weights, regularization_factor, y):
    return regularization_factor / len(y) * weights


layers = [784, 40, 10]
random_range = 0.0001
learn_rate = 0.1
sgd = 0.5
momentum_rate = 0.9
batch_size = 20
max_epochs = 30
regularization_rate = 0.1
dropout_factor = 0.8

autoencoder_activation_function = sigmoid
autoencoder_activation_derivative = d_sigmoid
autoencoder_cost_function = mst
autoencoder_gradient_function = mst_sigmoid_gradient
regularization_cost = regularization_l2_cost
regularization_gradient = regularization_l2_gradient

mlp_activation_function = softplus
mlp_activation_derivative = d_softplus
mlp_cost_function = log_likelihood
mlp_gradient_function = softmax_log_gradient


def load_data():
    with gzip.open('mnist.pkl.gz', 'rb') as f:
        return pickle.load(f, encoding='latin1')


if __name__ == '__main__':
    training_data, validation_data, test_data = load_data()

    print("### Learning autoencoder...")

    autoencoder_layers = np.copy(layers)
    autoencoder_layers[-1] = autoencoder_layers[0]

    autoencoder = net.Network(
        autoencoder_layers,
        random_range,
        autoencoder_activation_function,
        autoencoder_activation_derivative,
        autoencoder_cost_function,
        autoencoder_gradient_function,
        regularization_cost,
        regularization_gradient,
        use_dropout=True
    )

    training_input, training_output = training_data
    validation_input, validation_output = validation_data
    ae_training_errors, ae_validation_errors = autoencoder.train(
        learn_rate,
        sgd,
        momentum_rate,
        batch_size,
        max_epochs,
        (training_input, training_input),
        (validation_input, validation_input),
        regularization_rate,
        dropout_factor
    )

    print("### Learning MLP...")
    mlp = net.Network(
        layers,
        random_range,
        mlp_activation_function,
        mlp_activation_derivative,
        mlp_cost_function,
        mlp_gradient_function,
        regularization_cost,
        regularization_gradient=None,
        use_softmax=True
    )

    hidden_weights = autoencoder.weights[0]
    hidden_biases = autoencoder.biases[0]

    mlp.weights[0] = hidden_weights
    mlp.biases[0] = hidden_biases

    mlp_training_errors, mlp_validation_errors = mlp.train(
        learn_rate,
        sgd,
        momentum_rate,
        batch_size,
        max_epochs,
        training_data,
        validation_data,
        regularization_rate=0
    )

    print("### Evaluating resulting network...")
    test_input, test_output = test_data
    result = mlp.evaluate(test_input, test_output)
    print("Evaluation result: ", result)

    print("Saving network's weights and biases visualization...")
    autoencoder.save_weights_and_biases_to_file("ae_weights_and_biases")
    mlp.save_weights_and_biases_to_file("mlp_weights_and_biases")

    print("Printing graph with results...")
    tests = [
        (
            [
                'training_errors',
                'validation_errors',
            ],
            [*ae_training_errors, *mlp_training_errors],
            [*ae_validation_errors, *mlp_validation_errors],
        )
    ]
    colors = ['r', 'g', 'b', 'y', 'm']
    for test in tests:
        plt.figure()
        for i in range(1, len(test)):
            plt.plot(test[i], colors[i - 1])
        plt.legend(test[0])
        plt.ylabel('mean error value per neuron')
        plt.xlabel('epoch number [n]')

    plt.show()

    print("### END ###")
