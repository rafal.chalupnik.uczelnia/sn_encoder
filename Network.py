import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt


def generate_weights(rows, columns, random_range):
    return np.random.uniform(-random_range, random_range, (rows, columns))


def print_errors(epoch, training_error, validation_error, regularization_punishment):
    print("Epoch {:03d} errors: training data: {:.6f} | validation data: {:.6f} | regularization punishment: {:.6f}"
          .format(epoch, training_error, validation_error, regularization_punishment))


# def softmax(x):
#     e_x = np.exp(x - np.max(x))
#     return e_x / np.sum(e_x, axis=0)


def softmax(z):
    exp_z = np.exp(z - np.max(z, axis=1, keepdims=True))
    return exp_z / np.sum(exp_z, axis=1, keepdims=True)


class Network:
    def __init__(self, layers, random_range, activation_function, activation_derivative, cost_function, gradient_function, regularization_cost, regularization_gradient, use_softmax=False, use_dropout=False):
        self.layers_count = len(layers) - 1
        self.random_range = random_range

        self.weights = [generate_weights(x, y, random_range) for x, y in zip(layers[:-1], layers[1:])]
        self.biases = [generate_weights(1, y, random_range) for y in layers[1:]]

        self.activation_function = activation_function
        self.activation_derivative = activation_derivative
        self.cost_function = cost_function
        self.gradient_function = gradient_function
        self.regularization_cost = regularization_cost
        self.regularization_gradient = regularization_gradient
        self.use_softmax = use_softmax
        self.use_dropout = use_dropout

    def feed_forward(self, x, mask=None):
        z = []
        a = [x]

        if mask is not None:
            for w, b, m in zip(self.weights, self.biases, mask):
                z.append((a[-1] @ (w * m)) + b)
                a.append(self.activation_function(z[-1]))
        else:
            for w, b in zip(self.weights, self.biases):
                z.append((a[-1] @ w) + b)
                a.append(self.activation_function(z[-1]))

        if self.use_softmax:
            a[-1] = softmax(z[-1])

        return z, a

    def predict(self, x):
        feed = self.feed_forward(x)
        return feed[1][-1]

    def back_propagate(self, z, a, y):
        gradient = [self.gradient_function(z[-1], a[-1], y)]

        for i in range(self.layers_count - 2, -1, -1):
            gradient.append((gradient[-1] @ self.weights[i + 1].T) * self.activation_derivative(z[i]))
        gradient.reverse()

        return gradient

    def calculate_error(self, data):
        x, y = data
        predicted = self.predict(x)
        return self.cost_function(y, predicted)

    def train(self, learn_rate, sgd, momentum_rate, batch_size, max_epochs, training_data, validation_data, regularization_rate, dropout_factor=0.0):
        current_epoch = 0
        training_data_errors = []
        validation_data_errors = []
        regularization_punishments = []

        training_data_errors.append(self.calculate_error(training_data))
        validation_data_errors.append(self.calculate_error(validation_data))
        regularization_punishments.append(self.regularization_cost(self.weights, regularization_rate, training_data))

        print_errors(current_epoch, training_data_errors[-1], validation_data_errors[-1], regularization_punishments[-1])

        while current_epoch < max_epochs:
            current_epoch += 1
            prev_weights = self.weights.copy()
            prev_biases = self.biases.copy()

            zipped_data = list(zip(training_data[0], training_data[1]))
            np.random.shuffle(zipped_data)

            for batch in np.split(np.asarray(zipped_data), len(zipped_data) / batch_size):
                x, y = zip(*batch)
                active_neurons_mask = None

                if self.use_dropout:
                    active_neurons_mask = np.random.uniform(0, 1, self.biases[0].shape)
                    active_neurons_mask = np.where(active_neurons_mask < dropout_factor, 0, 1)#, 1, 0)
                    active_neurons_mask = [active_neurons_mask, active_neurons_mask.T]

                z, a = self.feed_forward(np.asarray(x), active_neurons_mask)
                gradient = self.back_propagate(z, a, np.asarray(y))

                for l in range(self.layers_count):
                    if active_neurons_mask is None:
                        # Training
                        self.weights[l] += learn_rate * (a[l].T @ gradient[l] / batch_size)
                        self.biases[l] += learn_rate * (np.sum(gradient[l], axis=0).reshape(self.biases[l].shape) / batch_size)
                    else:
                        # Training
                        self.weights[l] += learn_rate * (a[l].T @ gradient[l] / batch_size) * active_neurons_mask[l]
                        self.biases[l] += learn_rate * (np.sum(gradient[l], axis=0).reshape(self.biases[l].shape) / batch_size)

                    # Regularization
                    if self.regularization_gradient is not None:
                        self.weights[l] += learn_rate * (
                                    self.regularization_gradient(self.weights[l], regularization_rate,
                                                                 x) / batch_size)

                    # Momentum
                    # self.weights[l] += momentum_rate * (self.weights[l] - prev_weights[l])
                    # self.biases[l] += momentum_rate * (self.biases[l] - prev_weights[l])

            training_data_errors.append(self.calculate_error(training_data))
            validation_data_errors.append(self.calculate_error(validation_data))
            regularization_punishments.append(self.regularization_cost(self.weights, regularization_rate, training_data))

            print_errors(current_epoch, training_data_errors[-1], validation_data_errors[-1], regularization_punishments[-1])

            if len(training_data_errors) > 1 and training_data_errors[-1] > training_data_errors[-2]:
                learn_rate -= sgd * learn_rate

        return training_data_errors, validation_data_errors

    def evaluate(self, x, y):
        predicted = self.predict(x)
        hits = 0.0

        for pi, yi in zip(predicted, y):
            if np.argmax(pi) == yi:
                hits += 1.0

        return hits * 100.0 / len(y)

    def save_weights_and_biases_to_file(self, file_name):
        visual = [self.weights[0] + self.biases[0]]
        for w, b in zip(self.weights[1:], self.biases[1:]):
            visual.append((visual[-1] @ w) + b)

        for i in range(len(visual)):
            plt.imsave(file_name + '_in_layer_{}'.format(i + 1), np.vstack([neuron.reshape((28, 28)) for neuron in visual[i].T]), cmap = 'bwr')
